package com.soaint.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.soaint.demo.dto.ClienteRequest;
import com.soaint.demo.dto.JwtDto;
import com.soaint.demo.dto.JwtRequest;
import com.soaint.demo.dto.UserResponse;
import com.soaint.demo.jwt.JwtProvider;
import com.soaint.demo.service.AuthService;
import com.soaint.demo.util.Constant;

@RestController
@RequestMapping("${spring.data.rest.base-path}" + Constant.API_AUTH)
@CrossOrigin
public class AuthController {

	public static final Gson GSON = new GsonBuilder().serializeNulls().create();
	public static final Logger log = LoggerFactory.getLogger(AuthController.class);

	@Autowired
	private AuthService authService;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtProvider jwtProvider;

	@PostMapping("/createUser")
	public UserResponse createUser(@RequestBody ClienteRequest request) {
		log.info("saveUser - request: " + GSON.toJson(request));
		UserResponse response = new UserResponse();
		response.setStatus(Constant.SUCCESS);
		response.setUser(authService.saveUser(request));
		log.info("saveUser - response: " + GSON.toJson(response));
		return response;
	}

	@PostMapping("/generateToken")
	public ResponseEntity<JwtDto> generateToken(@RequestBody JwtRequest login) {
		log.info("generateToken - request: " + GSON.toJson(login));
		JwtDto jwtDto = new JwtDto();
		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(login.getUsername(), login.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtProvider.generateToken(authentication);
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		jwtDto.setToken(jwt);
		jwtDto.setUsername(userDetails.getUsername());
		log.info("generateToken - response: " + GSON.toJson(jwtDto));
		return new ResponseEntity<JwtDto>(jwtDto, HttpStatus.OK);
	}
	
	//@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/getUser")
	public UserResponse getUser(@RequestBody ClienteRequest request) {
		log.info("saveUser - request: " + GSON.toJson(request));
		UserResponse response = new UserResponse();
		response.setStatus(Constant.SUCCESS);
		response.setUser(authService.getUser(request));
		log.info("saveUser - response: " + GSON.toJson(response));
		return response;
	}

}
