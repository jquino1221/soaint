package com.soaint.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.soaint.demo.dto.ProductoRequest;
import com.soaint.demo.entity.ProductoEntity;
import com.soaint.demo.service.ProductoService;
import com.soaint.demo.util.Constant;

import io.reactivex.Single;

@RestController
@RequestMapping("${spring.data.rest.base-path}" + Constant.API_PRODUCTO)
@CrossOrigin
public class ProductoController {

	public static final Gson GSON = new GsonBuilder().serializeNulls().create();
	public static final Logger log = LoggerFactory.getLogger(ProductoController.class);

	@Autowired
	private ProductoService productoService;
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/create")
	public Single<ProductoEntity> create(@RequestBody ProductoRequest request) {
		log.info("createProducto - request: " + GSON.toJson(request));
		Single<ProductoEntity> response = productoService.create(request);
		log.info("createProducto - response: " + GSON.toJson(response));
		return response;
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/read")
	public ProductoEntity read(@RequestBody ProductoRequest request) {
		log.info("readProducto - request: " + GSON.toJson(request));
		ProductoEntity response = productoService.read(request);
		log.info("readProducto - response: " + GSON.toJson(response));
		return response;
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/update")
	public ProductoEntity update(@RequestBody ProductoRequest request) {
		log.info("updateProducto - request: " + GSON.toJson(request));
		ProductoEntity response = productoService.update(request);
		log.info("updateProducto - response: " + GSON.toJson(response));
		return response;
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/delete")
	public void delete(@RequestBody ProductoRequest request) {
		log.info("deleteProducto - request: " + GSON.toJson(request));
		productoService.delete(request);
	}

}
