package com.soaint.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.soaint.demo.dto.VentaRequest;
import com.soaint.demo.entity.DetalleVentaEntity;
import com.soaint.demo.entity.VentaEntity;
import com.soaint.demo.service.VentaService;
import com.soaint.demo.util.Constant;

import io.reactivex.Single;

@RestController
@RequestMapping("${spring.data.rest.base-path}" + Constant.API_VENTA)
@CrossOrigin
public class VentaController {

	public static final Gson GSON = new GsonBuilder().serializeNulls().create();
	public static final Logger log = LoggerFactory.getLogger(VentaController.class);

	@Autowired
	private VentaService ventaService;
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/create")
	public Single<VentaEntity> create(@RequestBody VentaRequest request) {
		log.info("createVenta - request: " + GSON.toJson(request));
		Single<VentaEntity> response = ventaService.create(request);
		log.info("createVenta - response: " + GSON.toJson(response));
		return response;
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/read")
	public Single<DetalleVentaEntity> read(@RequestBody VentaRequest request) {
		log.info("readVenta - request: " + GSON.toJson(request));
		Single<DetalleVentaEntity> response = ventaService.read(request);
		log.info("readVenta - response: " + GSON.toJson(response));
		return response;
	}
	
}
