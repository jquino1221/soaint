package com.soaint.demo.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.soaint.demo.dto.ProductoRequest;
import com.soaint.demo.entity.ProductoEntity;
import com.soaint.demo.repository.ProductoRepository;
import com.soaint.demo.service.ProductoService;

import io.reactivex.Single;

/**
 * Controlador principal que expone el servicio a trav&eacute;s de HTTP/Rest
 * para las operaciones de un usuario.<br/>
 *
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Juan Quino</li>
 * </ul>
 * 
 * @version 1.0
 */
@Service
public class ProductoServiceImpl implements ProductoService {

	public static final Gson GSON = new GsonBuilder().serializeNulls().create();
	public static final Logger log = LoggerFactory.getLogger(ProductoServiceImpl.class);

	@Autowired
	private ProductoRepository repository;

	@Override
	public Single<ProductoEntity> create(ProductoRequest entity) {
		ProductoEntity e = new ProductoEntity();
		e.setNombre(entity.getNombre());
		e.setPrecio(entity.getPrecio());
		return Single.just(repository.save(e));
	}

	@Override
	public ProductoEntity read(ProductoRequest request) {
		return repository.findByIdProducto(request.getIdProducto());
	}

	@Override
	public ProductoEntity update(ProductoRequest request) {
		ProductoEntity entity = repository.findByIdProducto(request.getIdProducto());
		if (entity != null) {
			entity.setIdProducto(request.getIdProducto());
			entity.setNombre(request.getNombre());
			entity.setPrecio(request.getPrecio());
		} else {
			throw new NullPointerException("No existe el producto");
		}
		return repository.save(entity);
	}

	@Override
	public void delete(ProductoRequest request) {
		repository.deleteById(request.getIdProducto());
	}

}
