package com.soaint.demo.service;

import com.soaint.demo.dto.ClienteRequest;
import com.soaint.demo.entity.ClienteEntity;

/**
 * Controlador principal que expone el servicio a trav&eacute;s de HTTP/Rest
 * para las operaciones de un usuario.<br/>
 *
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Juan Quino</li>
 * </ul>
 * 
 * @version 1.0
 */
public interface AuthService {

	ClienteEntity saveUser(ClienteRequest request);

	ClienteEntity getUser(ClienteRequest request);
	
}