package com.soaint.demo.service;

import com.soaint.demo.dto.VentaRequest;
import com.soaint.demo.entity.DetalleVentaEntity;
import com.soaint.demo.entity.VentaEntity;

import io.reactivex.Single;

public interface VentaService {

	Single<VentaEntity> create (VentaRequest entity);

	Single<DetalleVentaEntity> read(VentaRequest request);
	
}
