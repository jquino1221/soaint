package com.soaint.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.soaint.demo.dto.UsuarioPrincipal;
import com.soaint.demo.entity.ClienteEntity;
import com.soaint.demo.service.ClienteService;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    ClienteService clienteService;

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ClienteEntity usuario = clienteService.getByUsername(username);
        return UsuarioPrincipal.build(usuario);
    }
}
