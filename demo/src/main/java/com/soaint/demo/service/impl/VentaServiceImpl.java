package com.soaint.demo.service.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.soaint.demo.dto.VentaRequest;
import com.soaint.demo.entity.DetalleVentaEntity;
import com.soaint.demo.entity.VentaEntity;
import com.soaint.demo.repository.ClienteRepository;
import com.soaint.demo.repository.DetalleVentaRepository;
import com.soaint.demo.repository.ProductoRepository;
import com.soaint.demo.repository.VentaRepository;
import com.soaint.demo.service.VentaService;

import io.reactivex.Single;

/**
 * Controlador principal que expone el servicio a trav&eacute;s de HTTP/Rest
 * para las operaciones de un usuario.<br/>
 *
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Juan Quino</li>
 * </ul>
 * 
 * @version 1.0
 */
@Service
public class VentaServiceImpl implements VentaService {

	public static final Gson GSON = new GsonBuilder().serializeNulls().create();
	public static final Logger log = LoggerFactory.getLogger(VentaServiceImpl.class);

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private VentaRepository ventaRepository;

	@Autowired
	private ProductoRepository productoRepository;

	@Autowired
	private DetalleVentaRepository detalleVentaRepository;
	
	@Override
	public Single<VentaEntity> create(VentaRequest entity) {
		VentaEntity e = new VentaEntity();
		e.setCliente(clienteRepository.findByIdCliente(entity.getIdCliente()));
		e.setFecha(new Date());
		return Single.just(ventaRepository.save(e)).map(ex -> {
			DetalleVentaEntity detail = new DetalleVentaEntity();
			detail.setVenta(ex);
			detail.setProducto(productoRepository.findByIdProducto(entity.getIdProducto()));
			detalleVentaRepository.save(detail);
			return ex;
		});
	}

	@Override
	public Single<DetalleVentaEntity> read(VentaRequest request) {
		return Single.just(ventaRepository.findByIdVenta(request.getIdVenta())).map(e -> {
			return detalleVentaRepository.findByVenta(ventaRepository.findByIdVenta(e.getIdVenta()));
		});
	}

}
