package com.soaint.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.soaint.demo.entity.RolEntity;
import com.soaint.demo.repository.RolRepository;

@Service
@Transactional
public class RolService {

    @Autowired
    RolRepository rolRepository;

    public RolEntity getByRolNombre(String descripcion){
        return rolRepository.findByDescripcion(descripcion);
    }

    public void save(RolEntity rol){
        rolRepository.save(rol);
    }
}
