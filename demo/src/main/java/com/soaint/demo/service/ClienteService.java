package com.soaint.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.soaint.demo.entity.ClienteEntity;
import com.soaint.demo.repository.ClienteRepository;

@Service
@Transactional
public class ClienteService {

    @Autowired
    private ClienteRepository repository;

    public ClienteEntity findByIdCliente(Integer idCliente){
        return repository.findByIdCliente(idCliente);
    }
    
    public ClienteEntity getByUsername(String username){
        return repository.findByUsername(username);
    }
    
    public boolean existsByUsername(String username){
        return repository.existsByUsername(username);
    }

    public boolean existsByEmail(String email){
        return repository.existsByEmail(email);
    }

    public ClienteEntity save(ClienteEntity usuario){
        return repository.save(usuario);
    }
	
    public String getUsername(String username){
    	ClienteEntity entity = repository.findByUsername(username);
    	return entity.getUsername();
    }
    
}
