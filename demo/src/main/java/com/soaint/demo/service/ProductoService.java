package com.soaint.demo.service;

import com.soaint.demo.dto.ProductoRequest;
import com.soaint.demo.entity.ProductoEntity;

import io.reactivex.Single;

public interface ProductoService {

	Single<ProductoEntity> create (ProductoRequest entity);

	ProductoEntity read(ProductoRequest request);

	ProductoEntity update(ProductoRequest request);

	void delete(ProductoRequest request);
	
}
