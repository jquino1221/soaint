package com.soaint.demo.service.impl;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.soaint.demo.dto.ClienteRequest;
import com.soaint.demo.entity.ClienteEntity;
import com.soaint.demo.entity.RolEntity;
import com.soaint.demo.service.AuthService;
import com.soaint.demo.service.ClienteService;
import com.soaint.demo.service.RolService;
import com.soaint.demo.util.Constant;

/**
 * Controlador principal que expone el servicio a trav&eacute;s de HTTP/Rest
 * para las operaciones de un usuario.<br/>
 *
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Juan Quino</li>
 * </ul>
 * 
 * @version 1.0
 */
@Service
public class AuthServiceImpl implements AuthService {

	public static final Gson GSON = new GsonBuilder().serializeNulls().create();
	public static final Logger log = LoggerFactory.getLogger(AuthServiceImpl.class);

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private ClienteService clienteService;

	@Autowired
	private RolService rolService;
	
	public ClienteEntity saveUser(ClienteRequest request) {
		ClienteEntity usuario = new ClienteEntity();
		usuario.setNombre(request.getNombre());
		usuario.setApellido(request.getApellido());
		usuario.setDni(request.getDni());
		usuario.setTelefono(request.getTelefono());
		usuario.setEmail(request.getEmail());
		usuario.setUsername(request.getUsername());
		usuario.setPassword(passwordEncoder.encode(request.getPassword()));
		Set<RolEntity> roles = new HashSet<>();
		roles.add(rolService.getByRolNombre(Constant.ROLE_ADMIN));
		usuario.setRoles(roles);
		return clienteService.save(usuario);
	}

	@Override
	public ClienteEntity getUser(ClienteRequest request) {
		return clienteService.findByIdCliente(request.getIdCliente());
	}

}
