package com.soaint.demo.exception;

public class EcosistemaException extends Exception {

	private static final long serialVersionUID = -8933348606536861069L;
	
	private String response;

	public EcosistemaException(String response) {
		this.response = response;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
	
}
