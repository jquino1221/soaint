package com.soaint.demo.dto;

import java.util.Date;

/**
 * Controlador principal que expone el servicio a trav&eacute;s de HTTP/Rest
 * para las operaciones de un usuario.<br/>
 *
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Juan Quino</li>
 * </ul>
 * 
 * @version 1.0
 */
public class VentaRequest {
	
	private Integer idVenta;
	private Integer idCliente;
	private Integer idProducto;
	private Date fecha;
	
	public Integer getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}
	public Integer getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(Integer idVenta) {
		this.idVenta = idVenta;
	}
	public Integer getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

}
