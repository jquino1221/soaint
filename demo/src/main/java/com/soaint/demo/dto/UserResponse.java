package com.soaint.demo.dto;

import com.soaint.demo.entity.ClienteEntity;

public class UserResponse {

	private String status;
	private ClienteEntity user;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ClienteEntity getUser() {
		return user;
	}

	public void setUser(ClienteEntity user) {
		this.user = user;
	}

}
