package com.soaint.demo.dto;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.soaint.demo.entity.ClienteEntity;

public class UsuarioPrincipal implements UserDetails {
    
	private static final long serialVersionUID = -8093762021376869641L;
	
	private String nombres;
	private String apellidos;
    private String username;
    private String email;
    private String password;
    private Collection<? extends GrantedAuthority> authorities;

    public UsuarioPrincipal(String nombres, String apellidos, String username, String email, String password,
    		Collection<? extends GrantedAuthority> authorities) {
        this.nombres = nombres;
        this.apellidos= apellidos;
        this.username = username;
        this.email = email;
        this.password = password;
        this.authorities = authorities;
    }

    public static UsuarioPrincipal build(ClienteEntity usuario){
    	List<GrantedAuthority> authorities = usuario.getRoles().stream().map(
    			rol -> new SimpleGrantedAuthority(rol.getDescripcion())).collect(Collectors.toList());
        return new UsuarioPrincipal(usuario.getNombre(), usuario.getApellido(), usuario.getUsername(), 
        		usuario.getEmail(), usuario.getPassword(), authorities);
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public boolean isAccountNonExpired() {
        return true;
    }

    public boolean isAccountNonLocked() {
        return true;
    }

    public boolean isCredentialsNonExpired() {
        return true;
    }

    public boolean isEnabled() {
        return true;
    }

    public String getNombres() {
        return nombres;
    }

    public String getEmail() {
        return email;
    }

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

}
