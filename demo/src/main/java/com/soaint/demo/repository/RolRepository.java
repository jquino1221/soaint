package com.soaint.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.soaint.demo.entity.RolEntity;

@Repository
public interface RolRepository extends JpaRepository<RolEntity, Integer> {
	
    RolEntity findByDescripcion(String rolNombre);
}
