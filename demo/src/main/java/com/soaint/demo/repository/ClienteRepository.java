package com.soaint.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.soaint.demo.entity.ClienteEntity;

/**
 * Controlador principal que expone el servicio a trav&eacute;s de HTTP/Rest
 * para las operaciones de un usuario.<br/>
 *
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Juan Quino</li>
 * </ul>
 * 
 * @version 1.0
 */
@Repository
public interface ClienteRepository extends JpaRepository<ClienteEntity, Integer> {

	ClienteEntity findByIdCliente(Integer idCliente);

	ClienteEntity findByUsernameAndPassword(String username, String password);

	boolean existsByUsername(String username);

	boolean existsByEmail(String email);

	ClienteEntity findByUsername(String username);

	ClienteEntity findByEmail(String email);

}
