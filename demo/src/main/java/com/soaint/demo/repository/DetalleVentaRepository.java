package com.soaint.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.soaint.demo.entity.DetalleVentaEntity;
import com.soaint.demo.entity.VentaEntity;

/**
 * Controlador principal que expone el servicio a trav&eacute;s de HTTP/Rest
 * para las operaciones de un usuario.<br/>
 *
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Juan Quino</li>
 * </ul>
 * 
 * @version 1.0
 */
@Repository
public interface DetalleVentaRepository extends JpaRepository<DetalleVentaEntity, Integer> {

	DetalleVentaEntity findByIdDetalleVenta (Integer idDetalleVenta);

	DetalleVentaEntity findByVenta (VentaEntity venta);
}
