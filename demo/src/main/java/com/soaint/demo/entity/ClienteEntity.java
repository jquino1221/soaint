package com.soaint.demo.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * Controlador principal que expone el servicio a trav&eacute;s de HTTP/Rest
 * para las operaciones de un usuario.<br/>
 *
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Juan Quino</li>
 * </ul>
 * 
 * @version 1.0
 */
@Entity
@Table(name="cliente")
public class ClienteEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idCliente", nullable = false)
	private Integer idCliente;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "detail_cliente_rol", 
	joinColumns = @JoinColumn(name = "idCliente"), 
	inverseJoinColumns = @JoinColumn(name = "idRol"))
	private Set<RolEntity> roles = new HashSet<>();

	@Column(name="nombre", nullable = true)
	private String nombre;
	
	@Column(name="apellido", nullable = true)
	private String apellido;

	@Column(name="dni", nullable = false)
	private Integer dni;
	
	@Column(name="telefono", nullable = true)
	private String telefono;
	
	@Column(name="email", nullable = false)
	private String email;

	@Column(name="username", nullable = false)
	private String username;
	
	@Column(name="password", nullable = false)
	private String password;

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Integer getDni() {
		return dni;
	}

	public void setDni(Integer dni) {
		this.dni = dni;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<RolEntity> getRoles() {
		return roles;
	}

	public void setRoles(Set<RolEntity> roles) {
		this.roles = roles;
	}
	
}
