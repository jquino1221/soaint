package com.soaint.demo.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controlador principal que expone el servicio a trav&eacute;s de HTTP/Rest
 * para las operaciones de un usuario.<br/>
 *
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Juan Quino</li>
 * </ul>
 * 
 * @version 1.0
 */
public class Util {

	public static final Logger log = LoggerFactory.getLogger(Util.class);

	public static String NUMEROS = "0123456789";
	public static String MAYUSCULAS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public static String MINUSCULAS = "abcdefghijklmnopqrstuvwxyz";
	public static String ESPECIALES = "ñÑ";

	public static Date dateFormat(String inputDate, String format) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		try {
			if (inputDate != null) {
				return dateFormat.parse(inputDate);
			}
		} catch (Exception e) {
			log.info(e.getMessage());
		}
		return null;
	}

	public static String dateFormat(Date inputDate, String format) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		try {
			if (inputDate != null) {
				return dateFormat.format(inputDate);
			}
		} catch (Exception e) {
			log.info(e.getMessage());
		}
		return null;
	}
	
	public static String passwordGenerator() {
		String pswd = "";
		String key = MINUSCULAS + MAYUSCULAS + ESPECIALES;
		int length = 12;
		for (int i = 0; i < length; i++) {
			pswd += (key.charAt((int) (Math.random() * key.length())));
		}
		return pswd;
	}

	public static byte[] convertImagen(String imagen) {
		File file = new File(imagen);
		byte[] buffer = null;
		try {
			buffer = new byte[(int) file.length()];
		} catch (Exception e) {
			log.info(e.getLocalizedMessage());
		}
		return buffer;
	}
	
	public static void writeToFile(InputStream uploadedInputStream, String uploadedFileLocation) {
		try {
			OutputStream out = new FileOutputStream(new File(uploadedFileLocation));
			int read = 0;
			byte[] bytes = new byte[1024];

			out = new FileOutputStream(new File(uploadedFileLocation));
			while ((read = uploadedInputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
