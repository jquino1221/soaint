package com.soaint.demo.util;

/**
 * Controlador principal que expone el servicio a trav&eacute;s de HTTP/Rest
 * para las operaciones de un usuario.<br/>
 *
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Juan Quino</li>
 * </ul>
 * 
 * @version 1.0
 */
public class Constant {

	public static final String API_AUTH = "/auth";
	public static final String API_PRODUCTO = "/producto";
	public static final String API_VENTA = "/venta";
	
	public static final Boolean AVATAR_IS_URL = Boolean.TRUE;
	public static final Boolean AVATAR_NOT_URL = Boolean.FALSE;
	public static final Boolean FLAG_HABILITADO = Boolean.TRUE;
	public static final Boolean FLAG_DESABILITADO = Boolean.FALSE;

	public static final String FORMAT_DATE_YYYY_MM_DD = "yyyy-MM-dd";
	public static final String FORMAT_DATE_YYYY_MM_DD_HH_MM_SS = "dd-MM-yyyy HH:mm:ss";
	public static final String FORMAT_DATE_DD_MM_YYYY = "dd-MM-yyyy";
	public static final String FORMAT_DATE_CHAT_DD_MM_YYYY = "HH:mm:ss | MM-dd";
	
	public static final String PARAMETRO_CREACION_COMUNIDADES = "PARAMETRO_CREACION_COMUNIDADES";
	public static final String PARAMETRO_ABANDONAR_COMUNIDADES = "PARAMETRO_ABANDONAR_COMUNIDADES";
	public static final String PARAMETRO_ABANDONAR_RED_AMIGOS = "PARAMETRO_ABANDONAR_RED_AMIGOS";

	public static final String ADMIN = "Administrador";
	public static final String USER = "Miembro";
	public static final String ROLE_ADMIN = "ROLE_ADMIN";
	public static final String ROLE_USER = "ROLE_USER";

	public static final String SUCCESS = "SUCCESS";
	public static final String ERROR = "ERROR";
	public static final String INVALID_USERNAME = "Username invalido o ya existe";
	public static final String INVALID_EMAIL = "Email invalido o ya existe";
	public static final String INVALID_DOCUMENTO = "Documento invalido o ya existe";
	public static final String INVALID_TYPEDOCUMENT = "Tipo documento invalido";

	public static final String FORO_PROGRAMADO = "Programado";
	public static final String FORO_PERPETUO = "Perpetuo";

	public static final String ACUERDO_APROBADO = "Aprobado";
	public static final String ACUERDO_PENDIENTE = "Pendiente";

	public static final String SOLICITUD_INGRESO_CAUSA = "Solicitud ingreso a causa";
	public static final String SOLICITUD_INGRESO_COMUNIDAD = "Solicitud ingreso a comunidad";
	public static final String SOLICITUD_INGRESO_FORO = "Solicitud ingreso a foro";
	public static final String SOLICITUD_INGRESO_REDAMIGOS = "Solicitud ingreso a red amigos";
	public static final String SOLICITUD_COMPRA = "Solicitud de compra";
	public static final String REPORTE_FORO = "Reporte de foro";
	public static final String SOLICITUD_PENDIENTE = "Pendiente";
	public static final String SOLICITUD_ATENDIDA = "Atendida";
	public static final String SOLICITUD_RECHAZADA = "Rechazada";
	public static final String INVITACION_REDAMIGOS = "Invitacion unirse a red amigos";
	public static final String INVITACION_COMUNIDAD = "Invitacion unirse a la comunidad";
	public static final String VALIDACION_ACUERDO_COMUNIDAD = "Validacion de acuerdo comunidad";

	public static final Integer SOLICITUD_INGRESO_CAUSA_ID = 1;
	public static final Integer SOLICITUD_INGRESO_COMUNIDAD_ID = 2;
	public static final Integer SOLICITUD_INGRESO_FORO_ID = 3;
	public static final Integer SOLICITUD_INGRESO_REDAMIGOS_ID = 4;
	public static final Integer SOLICITUD_COMPRA_ID = 5;
	public static final Integer REPORTE_FORO_ID = 6;
	public static final Integer INVITACION_INGRESO_REDAMIGOS_ID = 7;
	public static final Integer INVITACION_INGRESO_COMUNIDAD_ID = 8;
	public static final Integer VALIDACION_ACUERDO_COMUNIDAD_ID = 9;

	public static final Integer CREAR_CUENTA_VALUE = 20;
	public static final Integer CREAR_CAUSA_VALUE = 20;
	public static final Integer COMENTAR_CAUSA_VALUE = 5;
	public static final Integer COMPROMETERSE_CAUSA_VALUE = 10;
	public static final Integer CREAR_FORO_VALUE = 20;
	public static final Integer CREAR_HILO_FORO_VALUE = 1;
	public static final Integer INVITACION_RED_AMIGOS_VALUE = 2;
	public static final Integer UNIRSE_RED_AMIGOS_VALUE = 1;
	public static final Integer CREAR_COMUNIDAD_VALUE = 20;
	public static final Integer UNIRSE_COMUNIDAD_VALUE = 5;

	public static final String CREAR_CUENTA = "Creacion de usuario";
	public static final String CREAR_CAUSA = "Creacion de la causa";
	public static final String COMENTAR_CAUSA = "Comentario de la causa";
	public static final String COMPROMETERSE_CAUSA = "Compromiso de la causa";
	public static final String CREAR_FORO = "Creacion de foro";
	public static final String CREAR_HILO_FORO = "Creacion de hilo de foro";
	public static final String INVITACION_RED_AMIGOS = "Invitacion de red de amigos";
	public static final String UNIRSE_RED_AMIGOS = "Unirse red de amigos";
	public static final String CREAR_COMUNIDAD = "Creacion de la comunidad";
	public static final String UNIRSE_COMUNIDAD = "Unirse comunidad";
	public static final String COMPRA_SOUVENIRS = "Compra de souvenirs";
	
	//public static final String PATH_IMG = "C:\\WordVision\\apache-tomcat-8.5.58-windows-x64\\apache-tomcat-8.5.58\\webapps\\business.ux.worldvision\\WEB-INF\\classes\\static\\img\\";
	//public static final String PATH_FILE = "C:\\WordVision\\apache-tomcat-8.5.58-windows-x64\\apache-tomcat-8.5.58\\webapps\\business.ux.worldvision\\WEB-INF\\classes\\static\\file\\";
	public static final String PATH_IMG = "C:\\Tomcat\\apache-tomcat-8.5.59-windows-x64\\apache-tomcat-8.5.59\\webapps\\business.ux.worldvision\\WEB-INF\\classes\\static\\img\\";
	public static final String PATH_FILE = "C:\\Tomcat\\apache-tomcat-8.5.59-windows-x64\\apache-tomcat-8.5.59\\webapps\\business.ux.worldvision\\WEB-INF\\classes\\static\\file\\";
	public static final String PATH_YOUTUBE = "https://www.youtube.com/embed/";

}
