package com.soaint.demo;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

import com.soaint.demo.dto.ClienteRequest;
import com.soaint.demo.dto.JwtDto;

@SpringBootTest
class ProductoEntityTests {

	@Test
	public void generateToken() {
		RestTemplate restTemplate = new RestTemplate();
		ClienteRequest request = new ClienteRequest();
		request.setUsername("jquino");
		request.setPassword("jquino");
		String url = "http://localhost:8080/soaint/auth/generateToken";
		JwtDto response = restTemplate.postForObject(url, request, JwtDto.class);
		Assert.assertEquals("jquino", response.getUsername());
	}

}
